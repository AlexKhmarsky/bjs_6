// Теоретичні питання
// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування?
//    Екранування - спеціальні символи в регулярних виразах, робить роботу з виразами більш зручною, часто використовуються.
// 2. Які засоби оголошення функцій ви знаєте?
//    Оголошення функції відбувається ключовим словом function, далі іде назва функції і її параметри
//    в круглих дужках та вираз функції в фігурних дужках. Є три вида оголошення - function definition,
//    или function declaration, или function statement.
// 3. Що таке hoisting, як він працює для змінних та функцій?
//    Це механізм в JS який дозволяє змінні і оголошення функцій здвинути вверх перед своєю областю видимості
//    перед тим як буде виконаний код. Механізм пересуває тільки оголошення функції та змінної.
//     Завдання
//     Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
//     Технічні вимоги:
//     Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
//     При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
//     Створити метод getAge() який повертатиме скільки користувачеві років.
//     Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

function createNewUser () {

    this.firstName = prompt('Enter your name.', '');

    Object.defineProperty(this, 'firstName', {
        writable: false
    });

this.setFirstName = function (newName) {

    Object.defineProperty(this, 'firstName', {
        writable: true
    });

    this.firstName = newName;

    Object.defineProperty(this, 'firstName', {
        writable: false
    });
    return newName;
};

this.surName = prompt('Enter your surname.', '');

Object.defineProperty(this, 'surName', {
    writable: false
    });

this.setSurName = function (newName) {
    Object.defineProperty(this, 'surName', {
        writable: true
    });
    this.surName = newName;

    Object.defineProperty(this, 'surName', {
        writable: false
    });
    return newName;
};

this.getLogin = function () {
    return (this.firstName[0] + this.surName).toLowerCase();
};

let userBirthday = prompt('Enter your birthday: dd.mm.yyyy.', 'dd.mm.yyyy').split('.');
this.birthday = new Date(+userBirthday[2], +userBirthday[1] - 1, +userBirthday[0]);
this.getUserAge = function () {
    let point = new Date();
    let userAge = (point.getFullYear() - this.birthday.getFullYear());
    if (point.getMonth() < this.birthday.getMonth() || (point.getMonth() === this.birthday.getMonth() && point.getDate() < this.birthday.getDate())) {
        --userAge;
    }
    return userAge;
};
this.getPassword = function () {
    return (this.firstName[0].toUpperCase() + this.surName.toLowerCase() + this.birthday.getFullYear());
};

}
let newUser = new createNewUser();

console.log(`User name: ${newUser.firstName}\nUser surname: ${newUser.surName}`);
console.log(`User age: ${newUser.getUserAge()}`);
console.log(`User password: ${newUser.getPassword()}`);
